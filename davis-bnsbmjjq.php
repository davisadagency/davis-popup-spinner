<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://davisadagency.com/auto-bnsbmjjq/
 * @since             1.0.0
 * @package           Davis_Gsxgroov
 *
 * @wordpress-plugin
 * Plugin Name:       Davis Spinner Popup
 * Plugin URI:        https://davisadagency.com/
 * Description:       Adds a custom spinner popup with prizes
 * Version:           1.2.5
 * Author:            Will Hart
 * Author URI:        https://davisadagency.com/
 * License:           All rights reserved 
 * Text Domain:       davis-spinner-popup
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('DAVIS_LXCLWTTA_VERSION', '1.2.5');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-davis-bnsbmjjq-activator.php
 */
function activate_davis_bnsbmjjq()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-davis-bnsbmjjq-activator.php';
    Davis_Gsxgroov_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-davis-bnsbmjjq-deactivator.php
 */
function deactivate_davis_bnsbmjjq()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-davis-bnsbmjjq-deactivator.php';
    Davis_Gsxgroov_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_davis_bnsbmjjq');
register_deactivation_hook(__FILE__, 'deactivate_davis_bnsbmjjq');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-davis-bnsbmjjq.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_davis_bnsbmjjq()
{

    $plugin = new Davis_Gsxgroov();
    $plugin->run();
}
run_davis_bnsbmjjq();

/**
 * Plugin Updater
 *
 * @since    1.0.0
 */

require plugin_dir_path(__FILE__) . 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/davisadagency/davis-popup-spinner',
    __FILE__,
    'davis-popup-spinner'
);
