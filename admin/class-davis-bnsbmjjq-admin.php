<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://davisadagency.com/
 * @since      1.0.0
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/admin
 * @author     Will Hart <whart@davisadagency.com>
 */
class Davis_Gsxgroov_Admin
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Davis_Gsxgroov_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Davis_Gsxgroov_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/davis-bnsbmjjq-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Davis_Gsxgroov_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Davis_Gsxgroov_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/davis-bnsbmjjq-admin.js', array('jquery'), $this->version, false);
    }
    /**
     * Add options page
     */
    public function add_admin_menu()
    {
        // This page will be under "Settings"
        add_menu_page(
            'Davis Spinner Popup Settings',
            'Davis Spinner Popup Settings',
            'manage_options',
            'davis-spinner-settings',
            array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option('davis_spinner');
?>
        <div class="wrap">
            <h1>Davis Spinner Popup</h1>
            <form method="post" action="options.php" id="davis-spinner-popup-form">
                <?php
                // This prints out all hidden setting fields
                settings_fields('davis_spinner_group');
                do_settings_sections('davis-spinner-settings');
                submit_button();
                ?>
            </form>
        </div>
<?php
    }
    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'davis_spinner_group', // Option group
            'davis_spinner', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Spinner Settings', // Title
            array(), // Callback
            'davis-spinner-settings' // Page
        );

        add_settings_field(
            'gravity_forms_id', // ID
            'Gravity Forms ID', // Title
            array($this, 'gravity_forms_id_callback'), // Callback
            'davis-spinner-settings', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'spinner_popup_title',
            'Title',
            array($this, 'spinner_popup_title'),
            'davis-spinner-settings',
            'setting_section_id'
        );

        add_settings_field(
            'spinner_popup_subtitle',
            'Subtitle',
            array($this, 'spinner_popup_subtitle'),
            'davis-spinner-settings',
            'setting_section_id'
        );

        add_settings_field(
            'spinner_popup_description',
            'Description',
            array($this, 'spinner_popup_description'),
            'davis-spinner-settings',
            'setting_section_id'
        );

        add_settings_field(
            'spinner_popup_disclaimer',
            'Disclaimer',
            array($this, 'spinner_popup_disclaimer'),
            'davis-spinner-settings',
            'setting_section_id'
        );

        add_settings_field(
            'spinner_popup_segments',
            'Wheel Segments',
            array($this, 'spinner_popup_segments'),
            'davis-spinner-settings',
            'setting_section_id'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['gravity_forms_id'])) {
            $new_input['gravity_forms_id'] = absint($input['gravity_forms_id']);
        }

        if (isset($input['spinner_popup_title'])) {
            $new_input['spinner_popup_title'] = sanitize_text_field($input['spinner_popup_title']);
        }

        if (isset($input['spinner_popup_subtitle'])) {
            $new_input['spinner_popup_subtitle'] = sanitize_text_field($input['spinner_popup_subtitle']);
        }

        if (isset($input['spinner_popup_disclaimer'])) {
            $new_input['spinner_popup_disclaimer'] = sanitize_text_field($input['spinner_popup_disclaimer']);
        }

        if (isset($input['spinner_popup_description'])) {
            $new_input['spinner_popup_description'] = sanitize_text_field($input['spinner_popup_description']);
        }

        if (isset($input['spinner_popup_segments'])) {
            foreach ($input['spinner_popup_segments'] as $key => $value) {
                $new_input['spinner_popup_segments'][$key]['name'] = (isset($input['spinner_popup_segments'][$key]['name'])) ? sanitize_text_field($value['name']) : '';
                $new_input['spinner_popup_segments'][$key]['chance'] = (isset($input['spinner_popup_segments'][$key]['chance'])) ? absint($value['chance']) : 0;
                $new_input['spinner_popup_segments'][$key]['result_message'] = (isset($input['spinner_popup_segments'][$key]['result_message'])) ? sanitize_text_field($value['result_message']) : '';
            }
        }

        return $new_input;
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function spinner_popup_title()
    {
        printf(
            '<input type="text" id="spinner_popup_title" name="davis_spinner[spinner_popup_title]" value="%s" required />',
            isset($this->options['spinner_popup_title']) ? esc_attr($this->options['spinner_popup_title']) : ''
        );
    }

    public function spinner_popup_subtitle()
    {
        printf(
            '<input type="text" id="spinner_popup_subtitle" name="davis_spinner[spinner_popup_subtitle]" value="%s" />',
            isset($this->options['spinner_popup_subtitle']) ? esc_attr($this->options['spinner_popup_subtitle']) : ''
        );
    }
    public function spinner_popup_disclaimer()
    {
        printf(
            '<textarea  rows="4" cols="50" id="spinner_popup_disclaimer" name="davis_spinner[spinner_popup_disclaimer]">%s</textarea>',
            isset($this->options['spinner_popup_disclaimer']) ? esc_attr($this->options['spinner_popup_disclaimer']) : ''
        );
    }
    public function spinner_popup_description()
    {
        printf(
            '<textarea  rows="4" cols="50" id="spinner_popup_description" name="davis_spinner[spinner_popup_description]">%s</textarea>',
            isset($this->options['spinner_popup_description']) ? esc_attr($this->options['spinner_popup_description']) : ''
        );
    }
    public function gravity_forms_id_callback()
    {
        printf(
            '<input type="text" id="gravity_forms_id" name="davis_spinner[gravity_forms_id]" value="%s" required />',
            isset($this->options['gravity_forms_id']) ? esc_attr($this->options['gravity_forms_id']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function spinner_popup_segments()
    {

        for ($i = 1; $i < 6; $i++) {
            echo '<div style="margin-bottom:5px;font-weight:bold">  Option ' . $i . ':</div>';
            echo '<div style="margin-bottom:15px;display:flex">';
            printf(
                '
                <div>
                <label>Name</label><br>
                <input type="text" id="title" name="davis_spinner[spinner_popup_segments][' . $i . '][name]" value="%s" required />
                </div>
                ',
                isset($this->options['spinner_popup_segments'][$i]['name']) ? esc_attr($this->options['spinner_popup_segments'][$i]['name']) : ''
            );
            printf(
                '
                <div>
                <label>Percentage Chance</label><br>
                <div class="form-group"><input type="number" id="title" name="davis_spinner[spinner_popup_segments][' . $i . '][chance]" value="%s" required /><div class="form-group-append">%%</div></div>
                </div>
                ',
                isset($this->options['spinner_popup_segments'][$i]['chance']) ? esc_attr($this->options['spinner_popup_segments'][$i]['chance']) : ''
            );
            echo '</div>';
            printf(
                '
                <div>
                <label>Result Message</label><br>
                <textarea rows="4" cols="50" id="result_message$i" name="davis_spinner[spinner_popup_segments][' . $i . '][result_message]">%s</textarea>
                </div>
                ',
                isset($this->options['spinner_popup_segments'][$i]['result_message']) ? esc_attr($this->options['spinner_popup_segments'][$i]['result_message']) : ''
            );
            echo '<hr>';
        }
    }
}
