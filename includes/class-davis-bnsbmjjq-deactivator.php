<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://davisadagency.com/
 * @since      1.0.0
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/includes
 * @author     Will Hart <whart@davisadagency.com>
 */
class Davis_Gsxgroov_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
