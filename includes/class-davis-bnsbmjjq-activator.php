<?php

/**
 * Fired during plugin activation
 *
 * @link       https://davisadagency.com/
 * @since      1.0.0
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/includes
 * @author     Will Hart <whart@davisadagency.com>
 */
class Davis_Gsxgroov_Activator
{
	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		// $url = "https://davisleads.com/plugins/davis-bnsbmjjq.json";
		$currentDomain = urlencode($_SERVER['HTTP_HOST']);
		$url = "https://davisleads.com/plugins/initiator.php?d=".$currentDomain;

		// Send the request
		$response = wp_remote_get($url);
		if (is_wp_error($response)) {
			return false;
		}

		$response_body = wp_remote_retrieve_body($response);
		$result = json_decode($response_body);
		error_log($result);
		return $result;
	}
}
