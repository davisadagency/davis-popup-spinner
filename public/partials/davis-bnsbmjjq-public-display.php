<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://davisadagency.com/
 * @since      1.0.0
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/public/partials
 */

$opt = get_option('davis_spinner');
$form_id = $opt['gravity_forms_id'];
$title = $opt['spinner_popup_title'];
$subtitle = $opt['spinner_popup_subtitle'];
$description = $opt['spinner_popup_description'];
$disclaimer = $opt['spinner_popup_disclaimer'];
?>
<?php
//if no form id has been set, don't show
if (!$form_id) {
  return;
} else { ?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="davis-dolls-modal">
  <div class="davis-dolls-overlay"></div>
  <div class="davis_dolls_wrapper">
    <div class="davis_dolls_close">&times; CLOSE</div>
    <div class="davis_dolls_bg">

      <div class="davis-dolls-container">
        <div class="davis-dolls-row">
          <div class="column-half">

            <div class="davis_dolls" align="center" valign="center">
              <div class="davis_dolls_inside">
                <canvas id="davis_canvas" class="davis_canvas" width="421" height="427" data-responsiveMinWidth="240" data-responsiveMaxWidth="400" data-responsiveScaleHeight="true" data-responsiveMargin="0">
                </canvas>
                <div class="davis_spin" alt="spin"></div>
                <div class="davis_prize_pointer" alt="<"></div>
              </div>
            </div>
          </div>
          <div class="column-half">
            <div class="davis-dolls-h1"><?php echo $title ?></div>
            <div class="davis-dolls-h3"><?php echo $subtitle ?></div>
            <div class="davis-dolls-copy">
              <p class="davis-dolls-p"><?php echo $description ?></p>
            </div>

            <div class="power_controls">
              <?php gravity_form($form_id, false, false, false, '', true, 12); ?>
            </div>
          </div>
        </div>
        <div class="davis-dolls-disclaimer">
          <?php echo $disclaimer ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>