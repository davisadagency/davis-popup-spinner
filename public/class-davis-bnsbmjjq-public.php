<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://davisadagency.com/
 * @since      1.0.0
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Davis_Gsxgroov
 * @subpackage Davis_Gsxgroov/public
 * @author     Will Hart <whart@davisadagency.com>
 */
class Davis_Gsxgroov_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * The setting options
     *
     * @since    1.0.0
     * @access   private
     * @var      array    $options    The current settings of this plugin.
     */
    private $options;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->options = get_option('davis_spinner');
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Davis_Gsxgroov_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Davis_Gsxgroov_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if (is_front_page()) {

            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/davis-bnsbmjjq-public.css', array(), $this->version, 'all');
            
        }
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Davis_Gsxgroov_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Davis_Gsxgroov_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        if (is_front_page()) {

            wp_enqueue_script('tweenmax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js', array(), $this->version, true);
            wp_enqueue_script('bnsbmjjq', plugin_dir_url(__FILE__) . 'js/davis-bnsbmjjq-base.js', array('tweenmax', 'jquery'), $this->version, true);
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/davis-bnsbmjjq-public.js', array('bnsbmjjq'), $this->version, true);
            wp_localize_script($this->plugin_name, 'php_vars', $this->options);
        }
    }

    public function display_html()
    {
        if (is_front_page()) {
            $path = plugin_dir_path(__FILE__) . 'partials/davis-bnsbmjjq-public-display.php';
            require $path;
        }
    }

    public function gravity_form_enqueue_scripts()
    {
        if (is_front_page()) {
            gravity_form_enqueue_scripts(1, true);
        }
    }
}
